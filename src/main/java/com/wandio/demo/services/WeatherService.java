package com.wandio.demo.services;

import com.wandio.demo.exceptions.CantBeCalculatedAverage;
import com.wandio.demo.exceptions.WeatherServiceHasProblemException;
import com.wandio.demo.helpers.CustomMath;
import com.wandio.demo.models.view.weather.WeatherAverageViewModel;
import com.wandio.demo.models.view.weather.WeatherService1ViewModel;
import com.wandio.demo.models.view.weather.WeatherService2ViewModel;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class WeatherService {

    @Value("${weather.service.first}")
    private String weatherServiceFirst;

    @Value("${weather.service.second}")
    private String weatherServiceSecond;


    public WeatherAverageViewModel callWeatherServicesAndCalculateAverageMaxTemperature() {
        WeatherAverageViewModel weatherAverageViewModel = new WeatherAverageViewModel();
        WeatherService1ViewModel weatherService1ViewMode = fetchDataFromFirstWeatherService();
        WeatherService2ViewModel weatherService2ViewMode = fetchDataFromSecondWeatherService();

        Double maxTempService1 = weatherService1ViewMode.getMaxTemp();
        Double maxTempService2 = null;

        if (weatherService2ViewMode.getMain() != null) {
            maxTempService2 = weatherService2ViewMode.getMain().getTempMax();
        }

        if (maxTempService1 == null && maxTempService2 == null) {
            throw new CantBeCalculatedAverage("Can't be calculated average, one of the Temp is null!");
        }

        weatherAverageViewModel.setWeatherAverage(CustomMath.calculateAverage(maxTempService1, maxTempService2));
        return weatherAverageViewModel;
    }

    private WeatherService1ViewModel fetchDataFromFirstWeatherService() {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<WeatherService1ViewModel> result = restTemplate.exchange(weatherServiceFirst, HttpMethod.GET, null, WeatherService1ViewModel.class);
        if (!result.getStatusCode().is2xxSuccessful()) {
            throw new WeatherServiceHasProblemException(weatherServiceFirst + "has some problem !");
        }
        return result.getBody();
    }

    private WeatherService2ViewModel fetchDataFromSecondWeatherService() {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<WeatherService2ViewModel> result = restTemplate.exchange(weatherServiceSecond, HttpMethod.GET, null, WeatherService2ViewModel.class);
        if (!result.getStatusCode().is2xxSuccessful()) {
            throw new WeatherServiceHasProblemException(weatherServiceSecond + "has some problem !");
        }
        return result.getBody();
    }
}
