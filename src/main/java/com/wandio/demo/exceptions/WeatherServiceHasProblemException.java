package com.wandio.demo.exceptions;

public class WeatherServiceHasProblemException extends RuntimeException {
    public WeatherServiceHasProblemException(String message) {
        super(message);
    }
}
