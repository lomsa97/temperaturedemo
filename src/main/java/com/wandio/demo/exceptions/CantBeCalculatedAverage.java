package com.wandio.demo.exceptions;

public class CantBeCalculatedAverage extends RuntimeException {

    public CantBeCalculatedAverage(String message) {
        super(message);
    }
}
