package com.wandio.demo.models.view.weather.weatherservice.details;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SysViewModel {

    private Double message;
    private String country;
    private Double sunrise;
    private Double sunset;
}
