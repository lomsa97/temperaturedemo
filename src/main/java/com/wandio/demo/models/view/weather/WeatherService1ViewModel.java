package com.wandio.demo.models.view.weather;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class WeatherService1ViewModel {

    private Long id;
    @JsonProperty("weather_state_name")
    private String stateName;
    @JsonProperty("weather_state_abbr")
    private String stateAbbr;
    @JsonProperty("wind_direction_compass")
    private String direction_compass;
    @JsonProperty("created")
    private LocalDateTime created;
    @JsonProperty("applicable_date")
    private LocalDate applicableDate;
    @JsonProperty("min_temp")
    private Double minTemp;
    @JsonProperty("max_temp")
    private Double maxTemp;
    @JsonProperty("the_temp")
    private Double theTemp;
    @JsonProperty("wind_speed")
    private Double windSpeed;
    @JsonProperty("wind_direction")
    private Double windDirection;
    @JsonProperty("air_pressure")
    private Double airPressure;
    @JsonProperty("humidity")
    private Double humidity;
    @JsonProperty("visibility")
    private Double visibility;
    @JsonProperty("predictability")
    private Double predictability;
}
