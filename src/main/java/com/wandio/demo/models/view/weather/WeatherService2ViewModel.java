package com.wandio.demo.models.view.weather;

import com.wandio.demo.models.view.weather.weatherservice.details.*;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class WeatherService2ViewModel {

    private Long id;
    private String name;
    private Long code;
    private CoordViewModel coord;
    private List<WeatherViewModel> weather;
    private String base;
    private MainViewModel main;
    private WindViewModel wind;
    private CloudsViewModel clouds;
    private Double dt;
    private SysViewModel sys;
}


