package com.wandio.demo.models.view.weather.weatherservice.details;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WeatherViewModel {

    private Long id;
    private String main;
    private String description;
    private String icon;
}
