package com.wandio.demo.models.view.weather.weatherservice.details;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class WindViewModel {
    private Double speed;
    private Double deg;
}
