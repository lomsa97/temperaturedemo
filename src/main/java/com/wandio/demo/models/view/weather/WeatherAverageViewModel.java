package com.wandio.demo.models.view.weather;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WeatherAverageViewModel {
    private Double weatherAverage;
}
