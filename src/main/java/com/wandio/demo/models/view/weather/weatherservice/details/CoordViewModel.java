package com.wandio.demo.models.view.weather.weatherservice.details;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CoordViewModel {
    private Double lon;
    private Double lat;
}
