package com.wandio.demo.controllers;

import com.wandio.demo.helpers.Response;
import com.wandio.demo.models.view.weather.WeatherAverageViewModel;
import com.wandio.demo.services.WeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WeatherController {

    private final WeatherService weatherService;

    @Autowired
    public WeatherController(WeatherService weatherService) {
        this.weatherService = weatherService;
    }

    @GetMapping("/temperature")
    public Response<WeatherAverageViewModel> callWeatherServicesAndCalculateAverageMaxTemperature() {
        Response<WeatherAverageViewModel> response = new Response<>();

        try {
            response.setData(weatherService.callWeatherServicesAndCalculateAverageMaxTemperature());
            response.setSuccess(true);
        } catch (Exception ex) {
            response.setErrorMessage(ex.getMessage());
            response.setSuccess(false);
        }

        return response;
    }
}
