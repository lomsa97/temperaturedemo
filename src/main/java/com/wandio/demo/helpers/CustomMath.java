package com.wandio.demo.helpers;

import java.util.Arrays;

public class CustomMath {

    public static Double calculateAverage(Double... numbers) {
        return Arrays.stream(numbers).mapToDouble(Double::doubleValue).average().getAsDouble();
    }
}
