package com.wandio.demo.helpers;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Response<T> {

    private boolean success;
    private String errorMessage;
    private T data;
}
